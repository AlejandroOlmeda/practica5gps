package es.unex.giiis.gb02project;


import android.content.Context;
import android.content.SharedPreferences;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RatingBar;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.CoordinatesProvider;
import androidx.test.espresso.action.GeneralClickAction;
import androidx.test.espresso.action.Press;
import androidx.test.espresso.action.Tap;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import es.unex.giiis.gb02project.dataApp.roomdb.AppDatabase;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddRatingTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    //let's mock the preferences
    SharedPreferences.Editor preferencesEditor;

    @Before
    public void before() throws Exception {
        //take shared preferences, if necessary
        Context targetContext = getInstrumentation().getTargetContext();
        preferencesEditor = targetContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE).edit();

        preferencesEditor.putLong("UserID", 1).commit();
        preferencesEditor.putString("Username", "GB02 user").commit();

    }

    @After
    public void after() {
        preferencesEditor.remove("UserID").commit();
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(mActivityTestRule.getActivity()).getUsersRatingDao().deleteAll();
            }
        });
    }

    @Test
    public void addRatingTest() {


        ViewInteraction bottomNavigationItemView3 = onView(
                allOf(withId(R.id.navigation_explora), withContentDescription("Explora"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                1),
                        isDisplayed()));
        bottomNavigationItemView3.perform(click());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.recycler_view_explora),
                        childAtPosition(
                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                1)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.ratingBar)).perform(setRating(4.5f));

        pressBack();

        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.recycler_view_explora),
                        childAtPosition(
                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                1)));
        recyclerView3.perform(actionOnItemAtPosition(0, click()));

        onView(withId(R.id.ratingBar)).check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }


    private static ViewAction setRating(final float rating) {
    if (rating % 0.5 != 0) {
        throw new IllegalArgumentException("Rating must be multiple of 0.5f");
    }

    return new ViewAction() {
        @Override
        public Matcher<View> getConstraints() {
            return isAssignableFrom(RatingBar.class);
        }

        @Override
        public String getDescription() {
            return "Set rating on RatingBar in 0.5f increments";
        }

        @Override
        public void perform(UiController uiController, View view) {
            GeneralClickAction viewAction = new GeneralClickAction(
                    Tap.SINGLE,
                    new CoordinatesProvider() {
                        @Override
                        public float[] calculateCoordinates(View view) {
                            int[] locationOnScreen = new int[2];
                            view.getLocationOnScreen(locationOnScreen);
                            int screenX = locationOnScreen[0];
                            int screenY = locationOnScreen[1];
                            int numStars = ((RatingBar) view).getNumStars();
                            float widthPerStar = 1f * view.getWidth() / numStars;
                            float percent = rating / numStars;
                            float x = screenX + view.getWidth() * percent;
                            float y = screenY + view.getHeight() * 0.5f;
                            return new float[]{x - widthPerStar * 0.5f, y};
                        }
                    },
                    Press.FINGER,
                    InputDevice.SOURCE_UNKNOWN,
                    MotionEvent.BUTTON_PRIMARY
            );
            viewAction.perform(uiController, view);
        }
    };
}
}
