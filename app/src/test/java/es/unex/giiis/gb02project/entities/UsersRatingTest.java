package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersRating;

import static org.junit.Assert.*;

public class UsersRatingTest {

    private UsersRating instance;

    @Before
    public void setUpTest() throws Exception {
        instance = new UsersRating(1,2,new Float(3.3));
    }

    @Test
    public void getIDUserTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mIDUser");
        field.setAccessible(true);
        field.set(instance, 3);
        //when
        final long result = instance.getIDUser();
        //then
        assertEquals("field wasn't retrieved properly", result, 3);
    }

    @Test
    public void setIDUserTest() throws NoSuchFieldException, IllegalAccessException  {
        Long value = new Long(1);
        instance.setIDUser(value);
        final Field field = instance.getClass().getDeclaredField("mIDUser");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getIDGameTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        field.set(instance, 2);
        //when
        final long result = instance.getIDGame();
        //then
        assertEquals("field wasn't retrieved properly", result, 2);
    }

    @Test
    public void setIDGameTest() throws NoSuchFieldException, IllegalAccessException  {
        Long value = new Long(1);
        instance.setIDGame(value);
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
    
    @Test
    public void getRatingTest() throws NoSuchFieldException, IllegalAccessException  {
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        field.set(instance, new Float(3.3));
        //when
        final Float result = instance.getRating();
        //then
        assertEquals("field wasn't retrieved properly", result, new Float(3.3), 0.0001);
    }

    @Test
    public void setRatingTest() throws NoSuchFieldException, IllegalAccessException  {
        Float value = new Float(3.5);
        instance.setRating(value);
        final Field field = instance.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }



}