package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;

import static org.junit.Assert.*;

public class CustomGameTest {

    private CustomGame customGame;

    @Before
    public void setUp() throws Exception {
        customGame = new CustomGame(1, "texto","texto","texto","texto");
    }

    @Test
    public void getIDTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = customGame.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        long value = 1;
        field.set(customGame, value);


        //when
        final long result = customGame.getID();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setIDTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 1;

        customGame.setID(value);
        final Field field = customGame.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(customGame), value);
    }

    @Test
    public void getIDUTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = customGame.getClass().getDeclaredField("mIDU");
        field.setAccessible(true);
        long value = 1;
        field.set(customGame, value);


        //when
        final long result = customGame.getIDU();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setIDUTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 1;

        customGame.setIDU(value);
        final Field field = customGame.getClass().getDeclaredField("mIDU");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(customGame), value);
    }

    @Test
    public void getGamenameTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = customGame.getClass().getDeclaredField("mGamename");
        field.setAccessible(true);
        String value = "texto";
        field.set(customGame, value);


        //when
        final String result = customGame.getGamename();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setGamenameTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        customGame.setGamename(value);
        final Field field = customGame.getClass().getDeclaredField("mGamename");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(customGame), value);
    }

    @Test
    public void getPlatformTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = customGame.getClass().getDeclaredField("mPlatform");
        field.setAccessible(true);
        String value = "texto";
        field.set(customGame, value);


        //when
        final String result = customGame.getPlatform();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setPlatformTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        customGame.setPlatform(value);
        final Field field = customGame.getClass().getDeclaredField("mPlatform");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(customGame), value);
    }

    @Test
    public void getRatingTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = customGame.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        String value = "texto";
        field.set(customGame, value);


        //when
        final String result = customGame.getRating();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setRatingTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        customGame.setRating(value);
        final Field field = customGame.getClass().getDeclaredField("mRating");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(customGame), value);
    }

    @Test
    public void getGenreTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = customGame.getClass().getDeclaredField("mGenre");
        field.setAccessible(true);
        String value = "texto";
        field.set(customGame, value);


        //when
        final String result = customGame.getGenre();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setGenreTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        customGame.setGamename(value);
        final Field field = customGame.getClass().getDeclaredField("mGenre");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(customGame), value);
    }
}