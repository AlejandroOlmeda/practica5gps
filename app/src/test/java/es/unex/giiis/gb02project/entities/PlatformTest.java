package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;

import static org.junit.Assert.*;

public class PlatformTest {

    private Platform instance;

    @Before
    public void setUp() throws Exception {
        instance = new Platform(1, 2, "mName", 0);
    }

    @Test
    public void getIDGame() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        field.set(instance, new Integer(1));
        //when
        final Integer result = instance.getIDGame();
        //then
        assertEquals("field wasn't retrieved properly", result, new Integer(1));
    }

    @Test
    public void setIDGame() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        instance.setIDGame(value);
        final Field field = instance.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getIDPlatform() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mIDPlatform");
        field.setAccessible(true);
        field.set(instance, new Integer(1));
        //when
        final Integer result = instance.getIDPlatform();
        //then
        assertEquals("field wasn't retrieved properly", result, new Integer(1));
    }

    @Test
    public void setIDPlatform() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        instance.setIDPlatform(value);
        final Field field = instance.getClass().getDeclaredField("mIDPlatform");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getName();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("mName");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void packageIntent() {
    }
}