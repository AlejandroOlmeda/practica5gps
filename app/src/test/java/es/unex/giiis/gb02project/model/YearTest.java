package es.unex.giiis.gb02project.model;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.dataApp.model.Year;
import es.unex.giiis.gb02project.dataApp.model.Year_;

import static org.junit.Assert.*;

public class YearTest {

    @Test
    public void getFromTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("from");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getFrom();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setFromTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Year instance = new Year();
        instance.setFrom(value);
        final Field field = instance.getClass().getDeclaredField("from");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getToTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("to");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getTo();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setToTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Year instance = new Year();
        instance.setTo(value);
        final Field field = instance.getClass().getDeclaredField("to");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getFilterTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("filter");
        field.setAccessible(true);
        field.set(instance, "FilterExample");
        //when
        final String result = instance.getFilter();
        //then
        assertEquals("field wasn't retrieved properly", result, "FilterExample");
    }

    @Test
    public void setFilterTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "FilterExample";
        Year instance = new Year();
        instance.setFilter(value);
        final Field field = instance.getClass().getDeclaredField("filter");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getDecadeTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("decade");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getDecade();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setDecadeTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Year instance = new Year();
        instance.setDecade(value);
        final Field field = instance.getClass().getDeclaredField("decade");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getYearsTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("years");
        field.setAccessible(true);
        List<Year_> value = new ArrayList<>();
        field.set(instance, value);
        //when
        final List<Year_> result = instance.getYears();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setYearsTest() throws NoSuchFieldException, IllegalAccessException  {
        List<Year_> value = new ArrayList<>();
        Year instance = new Year();
        instance.setYears(value);
        final Field field = instance.getClass().getDeclaredField("years");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getNofollowTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("nofollow");
        field.setAccessible(true);
        field.set(instance, false);
        //when
        final Boolean result = instance.getNofollow();
        //then
        assertEquals("field wasn't retrieved properly", result, false);
    }

    @Test
    public void setNofollowTest() throws NoSuchFieldException, IllegalAccessException  {
        Boolean value = false;
        Year instance = new Year();
        instance.setNofollow(value);
        final Field field = instance.getClass().getDeclaredField("nofollow");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getCountTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year instance = new Year();
        final Field field = instance.getClass().getDeclaredField("count");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getCount();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setCountTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Year instance = new Year();
        instance.setCount(value);
        final Field field = instance.getClass().getDeclaredField("count");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}