package es.unex.giiis.gb02project.model;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Store;
import es.unex.giiis.gb02project.dataApp.model.Store_;

import static org.junit.Assert.*;

public class StoreTest {

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException  {
        final Store instance = new Store();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getId();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Store instance = new Store();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getStoreTest() throws NoSuchFieldException, IllegalAccessException  {
        final Store instance = new Store();
        final Field field = instance.getClass().getDeclaredField("store");
        field.setAccessible(true);
        final Store_ value = new Store_();
        field.set(instance, value);
        //when
        final Store_ result = instance.getStore();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setStoreTest() throws NoSuchFieldException, IllegalAccessException  {
        Store_ value = new Store_();
        Store instance = new Store();
        instance.setStore(value);
        final Field field = instance.getClass().getDeclaredField("store");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getUrlEnTest() throws NoSuchFieldException, IllegalAccessException  {
        final Store instance = new Store();
        final Field field = instance.getClass().getDeclaredField("urlEn");
        field.setAccessible(true);
        field.set(instance, "UrlEnExample");
        //when
        final String result = instance.getUrlEn();
        //then
        assertEquals("field wasn't retrieved properly", result, "UrlEnExample");
    }

    @Test
    public void setUrlEnTest() throws NoSuchFieldException, IllegalAccessException  {
        String value = "UrlEnExample";
        Store instance = new Store();
        instance.setUrlEn(value);
        final Field field = instance.getClass().getDeclaredField("urlEn");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getUrlRuTest() throws NoSuchFieldException, IllegalAccessException  {
        final Store instance = new Store();
        final Field field = instance.getClass().getDeclaredField("urlRu");
        field.setAccessible(true);
        final Object value = new Object();
        field.set(instance, value);
        //when
        final Object result = instance.getUrlRu();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setUrlRuTest() throws NoSuchFieldException, IllegalAccessException  {
        Object value = new Object();
        Store instance = new Store();
        instance.setUrlRu(value);
        final Field field = instance.getClass().getDeclaredField("urlRu");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}