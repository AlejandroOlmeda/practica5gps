package es.unex.giiis.gb02project.entities;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;

import static org.junit.Assert.*;

public class ImagesGameTest {

    private ImagesGame imagesGame;

    @Before
    public void setUp() throws Exception {
        imagesGame = new ImagesGame(1,1,"texto");
    }

    @Test
    public void setIDTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        imagesGame.setID(value);
        final Field field = imagesGame.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(imagesGame), value);
    }

    @Test
    public void setImageTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        imagesGame.setImage(value);
        final Field field = imagesGame.getClass().getDeclaredField("mImage");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(imagesGame), value);
    }

    @Test
    public void getIDTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = imagesGame.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        int value = 1;
        field.set(imagesGame, 1);


        //when
        final int result = imagesGame.getID();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void getImageTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = imagesGame.getClass().getDeclaredField("mImage");
        field.setAccessible(true);
        String value = "texto";
        field.set(imagesGame, value);


        //when
        final String result = imagesGame.getImage();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void getIDGameTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = imagesGame.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        int value = 1;
        field.set(imagesGame, 1);


        //when
        final int result = imagesGame.getIDGame();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setIDGameTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        imagesGame.setIDGame(value);
        final Field field = imagesGame.getClass().getDeclaredField("mIDGame");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(imagesGame), value);
    }

    @Test
    public void packageIntentTest() throws NoSuchFieldException, IllegalAccessException {
    }
}