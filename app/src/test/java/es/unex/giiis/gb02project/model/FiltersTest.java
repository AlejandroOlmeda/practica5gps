package es.unex.giiis.gb02project.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.dataApp.model.Filters;
import es.unex.giiis.gb02project.dataApp.model.Year;

import static org.junit.Assert.*;

public class FiltersTest {

    private Filters instance;

    @Before
    public void setUp() throws Exception {
        instance = new Filters();
    }

    @Test
    public void getYearsTest() throws NoSuchFieldException, IllegalAccessException{
        final Field field = instance.getClass().getDeclaredField("years");
        field.setAccessible(true);
        List<Year> value = new ArrayList<>();
        value.add(new Year());
        value.add(new Year());
        field.set(instance, value);
        assertEquals("Field wasn't retrieved properly", instance.getYears(), value);
    }

    @Test
    public void setYearsTest() throws NoSuchFieldException, IllegalAccessException{
        List<Year> value = new ArrayList<>();
        value.add(new Year());
        value.add(new Year());
        instance.setYears(value);
        final Field field = instance.getClass().getDeclaredField("years");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}