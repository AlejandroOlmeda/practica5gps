package es.unex.giiis.gb02project.model;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Year_;

import static org.junit.Assert.*;

public class Year_Test {

    @Test
    public void getYearTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year_ instance = new Year_();
        final Field field = instance.getClass().getDeclaredField("year");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getYear();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setYearTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Year_ instance = new Year_();
        instance.setYear(value);
        final Field field = instance.getClass().getDeclaredField("year");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getCountTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year_ instance = new Year_();
        final Field field = instance.getClass().getDeclaredField("count");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getCount();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setCountTest() throws NoSuchFieldException, IllegalAccessException  {
        Integer value = 1;
        Year_ instance = new Year_();
        instance.setCount(value);
        final Field field = instance.getClass().getDeclaredField("count");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getNofollowTest() throws NoSuchFieldException, IllegalAccessException  {
        final Year_ instance = new Year_();
        final Field field = instance.getClass().getDeclaredField("nofollow");
        field.setAccessible(true);
        field.set(instance, false);
        //when
        final Boolean result = instance.getNofollow();
        //then
        assertEquals("field wasn't retrieved properly", result, false);
    }

    @Test
    public void setNofollowTest() throws NoSuchFieldException, IllegalAccessException  {
        Boolean value = false;
        Year_ instance = new Year_();
        instance.setNofollow(value);
        final Field field = instance.getClass().getDeclaredField("nofollow");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}