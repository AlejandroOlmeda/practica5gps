package es.unex.giiis.gb02project.dataApp.roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersRating;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;

@Database(entities = {User.class, UsersCompleted.class, UsersPlaying.class, UsersWishlist.class,CustomGame.class, UsersRating.class, PopularGames.class, NewGames.class, Platform.class, PlatformCategory.class, Genre.class, ImagesGame.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static AppDatabase getInstance(Context context){
        if (instance == null)
            instance = Room.databaseBuilder(context, AppDatabase.class, "Apgb02.db").build();
        return instance;
    }

    public abstract UserDao getUserDao();

    public abstract UsersCompletedDao getUsersCompletedDao();

    public abstract UsersRatingDao getUsersRatingDao();

    public abstract  CustomGamesDao getCustomGamesDao();
    public abstract UsersPlayingDao getUsersPlayingDao();

    public abstract UsersWishlistDao getUsersWishlistDao();
    public abstract PopularDao getPopularDao();

    public abstract NewDao getNewDao();

    public abstract PlatformDao getPlatformDao();

    public abstract PlatformCategoryDao getPlatformCategoryDao();

    public abstract GenreDao getGenreDao();

    public abstract ImagesDao getImagesDao();
}
