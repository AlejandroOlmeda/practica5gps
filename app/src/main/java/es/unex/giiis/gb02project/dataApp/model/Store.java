
package es.unex.giiis.gb02project.dataApp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Store implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("store")
    @Expose
    private Store_ store;
    @SerializedName("url_en")
    @Expose
    private String urlEn;
    @SerializedName("url_ru")
    @Expose
    private Object urlRu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Store_ getStore() {
        return store;
    }

    public void setStore(Store_ store) {
        this.store = store;
    }

    public String getUrlEn() {
        return urlEn;
    }

    public void setUrlEn(String urlEn) {
        this.urlEn = urlEn;
    }

    public Object getUrlRu() {
        return urlRu;
    }

    public void setUrlRu(Object urlRu) {
        this.urlRu = urlRu;
    }

}
