package es.unex.giiis.gb02project.dataApp.roomdb.entities;

import android.content.Intent;

import androidx.room.*;

@Entity(tableName = "users")
public class User {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public final static String ID = "ID";
    @Ignore
    public final static String USERNAME = "username";
    @Ignore
    public final static String EMAIL = "email";
    @Ignore
    public final static String PASSWORD = "password";

    @PrimaryKey(autoGenerate = true)
    private long mID;
    @ColumnInfo(name = "username")
    private String mUsername = new String();
    @ColumnInfo(name = "email")
    private String mEmail = new String();
    @ColumnInfo(name = "password")
    private String mPassword = new String();

    @Ignore
    User(String username, String email, String password) {
        this.mUsername = username;
        this.mEmail = email;
        this.mPassword = password;
    }



    public User(long mID, String mUsername, String mEmail, String mPassword) {
        this.mID = mID;
        this.mUsername = mUsername;
        this.mEmail = mEmail;
        this.mPassword = mPassword;
    }

    // Create a new User from data packaged in an Intent

    @Ignore
    public User(Intent intent) {
        mID = intent.getLongExtra(User.ID,0);
        mUsername = intent.getStringExtra(User.USERNAME);
        mEmail = intent.getStringExtra(User.EMAIL);
        mPassword = intent.getStringExtra(User.PASSWORD);
    }

    public long getID() { return mID; }

    public void setID(long ID) { this.mID = ID; }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }




    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, String username,
                                     String email, String password) {

        intent.putExtra(User.USERNAME, username);
        intent.putExtra(User.EMAIL, email);
        intent.putExtra(User.PASSWORD, password);
    }

}
