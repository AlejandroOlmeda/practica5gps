package es.unex.giiis.gb02project.ui.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;
import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;

public class SearchViewModel extends ViewModel {

    private final ExploraRepository mRepository;
    private final LiveData<List<PlatformCategory>> mPlatformCategories;
    private final LiveData<List<Genre>> mGenres;

    public SearchViewModel(ExploraRepository exploraRepository) {
        mRepository = exploraRepository;
        mPlatformCategories = mRepository.getCurrentPlatformCategories();
        mGenres = mRepository.getCurrentGenres();
    }

    public void checkPlatformsGenres(){
        mRepository.getPlatforms();
        mRepository.getGenres();
    }

    public LiveData<List<PlatformCategory>> getPlatformCategories(){
        return mPlatformCategories;
    }

    public LiveData<List<Genre>> getGenres(){
        return mGenres;
    }

}
