package es.unex.giiis.gb02project.ui.configuracion;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;

public class ConfiguracionViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private PerfilRepository mPerfilRepository;

    public ConfiguracionViewModel(PerfilRepository perfilRepository) {
        mText = new MutableLiveData<>();
        mText.setValue("This is configuracion fragment");
        mPerfilRepository = perfilRepository;
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void deleteUser(long idUser){
        mPerfilRepository.deleteUser(idUser);
    }
}