package es.unex.giiis.gb02project.ui.gamedetails;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.ui.perfil.PerfilViewModel;

public class GameDetailsViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final PerfilRepository mRepository;

    public GameDetailsViewModelFactory(PerfilRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new GameDetailsViewModel(mRepository);
    }
}
