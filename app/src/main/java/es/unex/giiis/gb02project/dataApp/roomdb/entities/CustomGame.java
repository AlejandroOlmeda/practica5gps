package es.unex.giiis.gb02project.dataApp.roomdb.entities;

import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "custom_games")
public class CustomGame {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public final static String IDGame = "IDGame";
    @Ignore
    public final static String IDUser = "IDUser";
    @Ignore
    public final static String GAMENAME = "gamename";
    @Ignore
    public final static String PLATFORM = "platform";
    @Ignore
    public final static String RATING = "rating";
    @Ignore
    public final static String GENRE = "genre";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "IDGame")
    private long mID;

    @ColumnInfo(name = "IDUser")
    private long mIDU;

    @ColumnInfo(name = "gamename")
    private String mGamename = new String();

    @ColumnInfo(name = "platform")
    private String mPlatform = new String();

    @ColumnInfo(name = "rating")
    private String mRating = new String();

    @ColumnInfo(name ="genre")
    private String mGenre = new String();

    @Ignore
    public CustomGame(long mIDU, String mGamename, String mPlatform, String mRating, String mGenre) {
        this.mIDU = mIDU;
        this.mGamename = mGamename;
        this.mPlatform = mPlatform;
        this.mRating = mRating;
        this.mGenre = mGenre;
    }



    public CustomGame(long mID, long mIDU, String mGamename, String mPlatform, String mRating, String mGenre) {
        this.mID = mID;
        this.mIDU = mIDU;
        this.mGamename = mGamename;
        this.mPlatform = mPlatform;
        this.mRating = mRating;
        this.mGenre = mGenre;
    }

    // Create a new User from data packaged in an Intent

    @Ignore
    public CustomGame(Intent intent) {
        mID = intent.getLongExtra(CustomGame.IDGame,0);
        mIDU = intent.getLongExtra(CustomGame.IDUser, 0);
        mGamename = intent.getStringExtra(CustomGame.GAMENAME);
        mPlatform = intent.getStringExtra(CustomGame.PLATFORM);
        mRating = intent.getStringExtra(CustomGame.RATING);
        mGenre = intent.getStringExtra(CustomGame.GENRE);
    }

    public long getID() { return mID; }

    public void setID(long mID) { this.mID = mID; }

    public long getIDU() { return mIDU; }

    public void setIDU(long mIDU) { this.mIDU = mIDU; }

    public String getGamename() {
        return mGamename;
    }

    public void setGamename(String mGamename) {
        this.mGamename = mGamename;
    }

    public String getPlatform() {
        return mPlatform;
    }

    public void setPlatform(String mPlatform) {
        this.mPlatform = mPlatform;
    }
    public String getRating() {
        return mRating;
    }

    public void setRating(String mRating) {
        this.mRating = mRating;
    }

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String mGenre) {
        this.mGenre = mGenre;
    }


    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, long mIDU,
                                     String mGamename, String mPlatform, String mRating, String mGenre) {

        intent.putExtra(CustomGame.IDUser, mIDU);
        intent.putExtra(CustomGame.GAMENAME, mGamename);
        intent.putExtra(CustomGame.PLATFORM, mPlatform);
        intent.putExtra(CustomGame.RATING, mRating);
        intent.putExtra(CustomGame.GENRE, mGenre);
    }
}
