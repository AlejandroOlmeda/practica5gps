package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface NewDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<NewGames> newGames);

    @Query("SELECT * FROM new_games")
    public LiveData<List<NewGames>> getAll();

    @Insert
    public long insert(NewGames newGame);

    @Query("DELETE FROM new_games")
    public void deleteAll();

    @Query("SELECT * FROM new_games WHERE id = :id")
    public NewGames getByID(Integer id);

    @Query("SELECT count(id) FROM new_games")
    public Integer getNumberOfNewGames();

}
