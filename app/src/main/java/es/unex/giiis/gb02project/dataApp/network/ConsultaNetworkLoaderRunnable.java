package es.unex.giiis.gb02project.dataApp.network;

import java.io.IOException;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.Consulta;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConsultaNetworkLoaderRunnable implements Runnable{
    private final OnConsultaLoadedListener mOnConsultaLoadedListener;

    //true -> generos
    //false -> plataformas
    private final boolean mGenres_platforms;


    public ConsultaNetworkLoaderRunnable(boolean genres_platforms, OnConsultaLoadedListener onConsultaLoadedListener){
        mOnConsultaLoadedListener = onConsultaLoadedListener;
        mGenres_platforms = genres_platforms;
    }

    @Override
    public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rawg.io/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RawgService service = retrofit.create(RawgService.class);
        try {
            Consulta consulta;
            if(mGenres_platforms) {
                consulta = service.getConsultaGeneros().execute().body();
            }else{
                consulta = service.getConsultaPlataformas().execute().body();
            }

            AppExecutors.getInstance().mainThread().execute(() -> mOnConsultaLoadedListener.onConsultaLoaded(consulta));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Llamada al Listener con los datos obtenidos
    }
}
