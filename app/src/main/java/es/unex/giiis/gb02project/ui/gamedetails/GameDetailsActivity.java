package es.unex.giiis.gb02project.ui.gamedetails;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.adapters.ImagesAdapter;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.model.ShortScreenshot;
import es.unex.giiis.gb02project.dataApp.network.OnResultLoadedListener;

import es.unex.giiis.gb02project.dataApp.roomdb.UsersRatingDao;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersRating;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;
import es.unex.giiis.gb02project.ui.perfil.PerfilViewModel;

public class GameDetailsActivity extends AppCompatActivity implements ImagesAdapter.OnListInteractionListener{


    private RecyclerView recyclerViewImages;
    private RecyclerView.LayoutManager layoutManagerGallery;
    private ImagesAdapter adapter;
    private GameDetailsViewModel gameDetailsViewModel;
    private Result result;
    private long idu;
    private int idg;
    private boolean[] clickAdd = {false, false, false}; // Completados - Wishlist - Jugando ahora
    private boolean[] addGame = {true, true, true}; // Completados - Wishlist - Jugando ahora
    private UsersCompleted usersCompleted;
    private UsersPlaying usersPlaying;
    private UsersWishlist usersWishlist;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_details);

        SharedPreferences sp = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        result = (Result) getIntent().getSerializableExtra("game");
        idu = sp.getLong("UserID", -1);
        idg = result.getId();
        final Button bAddCompleted = findViewById(R.id.add_completados);
        final Button bAddWishlist = findViewById(R.id.add_wishlist);


        usersCompleted = new UsersCompleted(idu,idg, result.getName(), result.getBackgroundImage(), result.getRating(), result.getDescription(), result.getBackgroundImageAdditional());
        usersPlaying = new UsersPlaying(idu,idg, result.getName(), result.getBackgroundImage(), result.getRating(), result.getDescription(), result.getBackgroundImageAdditional());
        usersWishlist = new UsersWishlist(idu, idg, result.getName(), result.getBackgroundImage(), result.getRating(), result.getDescription(), result.getBackgroundImageAdditional());
        final RatingBar ratingBar = findViewById(R.id.ratingBar);
        final Context context = this;
        final Button bAddPlaying = findViewById(R.id.add_jugandoAhora);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        gameDetailsViewModel = new ViewModelProvider(this, appContainer.gameDetailsViewModelFactory).get(GameDetailsViewModel.class);

        recyclerViewImages = (RecyclerView)findViewById(R.id.photo_gallery);
        recyclerViewImages.setHasFixedSize(true);
        layoutManagerGallery = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImages.setLayoutManager(layoutManagerGallery);

        adapter = new ImagesAdapter(new ArrayList<>(), this, this);
        List<String> lurl = new ArrayList<>();



        gameDetailsViewModel.getPlaying().observe(this, new Observer<List<UsersPlaying>>() {
            @Override
            public void onChanged(List<UsersPlaying> usersPlayings) {
                addGame[2]=true;
                bAddPlaying.setTextColor(Color.parseColor("#353535"));
                for(UsersPlaying  uP: usersPlayings){
                    if(uP.getIDGame().intValue() == result.getId().intValue()){
                        addGame[2]=false;
                        bAddPlaying.setTextColor(Color.parseColor("#8B1B1B"));
                    }
                }
            }
        });


        gameDetailsViewModel.getCompleted().observe(this, new Observer<List<UsersCompleted>>() {
            @Override
            public void onChanged(List<UsersCompleted> usersCompleteds) {
                addGame[0]=true;
                bAddCompleted.setTextColor(Color.parseColor("#353535"));
                for(UsersCompleted  uC: usersCompleteds){
                    if(uC.getIDGame().intValue() == result.getId().intValue()){
                        addGame[0]=false;
                        bAddCompleted.setTextColor(Color.parseColor("#8B1B1B"));
                    }
                }
            }
        });

        gameDetailsViewModel.getWishlist().observe(this, new Observer<List<UsersWishlist>>() {
            @Override
            public void onChanged(List<UsersWishlist> usersWishlists) {
                addGame[1]=true;
                bAddWishlist.setTextColor(Color.parseColor("#353535"));
                for(UsersWishlist  uW: usersWishlists){
                    if(uW.getIDGame().intValue() == result.getId().intValue()){
                        addGame[1]=false;
                        bAddWishlist.setTextColor(Color.parseColor("#8B1B1B"));
                    }
                }
            }
        });

        gameDetailsViewModel.getImages().observe(this, new Observer<List<ImagesGame>>() {
            @Override
            public void onChanged(List<ImagesGame> imagesGames) {
                if (imagesGames != null) {
                    for (ImagesGame ss : imagesGames) {
                        if(ss.getIDGame().intValue() == result.getId().intValue()){
                            lurl.add(ss.getImage());
                        }

                    }
                    String aux = lurl.remove(0);
                    lurl.add(aux);
                }
            }
        });

        if(result.getBackgroundImageAdditional() != null) {
            lurl.add(result.getBackgroundImageAdditional());
            lurl.add(result.getBackgroundImage());
        }


        adapter.swap(lurl);
        recyclerViewImages.setAdapter(adapter);

        TextView textDescripcion = findViewById(R.id.text_descripcion);
        final String[] descripcion = {result.getDescription()};
        if (descripcion[0] != null) {
            descripcion[0] = descripcion[0].replace("<p>", "");
            descripcion[0] = descripcion[0].replace("<br />", "\n");
            descripcion[0] = descripcion[0].replace("</p>", "\n");
            descripcion[0] = descripcion[0].replace("&#39;s", "'");
        }

        textDescripcion.setText(descripcion[0]);
        ImageView iv = findViewById(R.id.image_toolbar);
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(result.getName());
        toolbar.setVisibility(View.INVISIBLE);

        RequestOptions requestOptions = new RequestOptions().centerCrop();

        Glide.with(this).asDrawable()
                .load(result.getBackgroundImage())
                .apply(requestOptions)
                .placeholder(R.drawable.loading)
                .into(iv);


        if (idu == -1){
            bAddCompleted.setVisibility(View.INVISIBLE);
            ratingBar.setVisibility(View.INVISIBLE);
            bAddPlaying.setVisibility(View.INVISIBLE);
            bAddWishlist.setVisibility(View.INVISIBLE);

        }
        else{
            bAddCompleted.setVisibility(View.VISIBLE);
            ratingBar.setVisibility(View.VISIBLE);
            bAddPlaying.setVisibility(View.VISIBLE);
            bAddWishlist.setVisibility(View.VISIBLE);
            ratingBar.setNumStars(5);

            gameDetailsViewModel.getRatingGameUser(idu, idg).observe(this, new Observer<UsersRating>() {
                @Override
                public void onChanged(UsersRating usersRating) {
                    if (usersRating != null)
                        ratingBar.setRating(usersRating.getRating());
                }
            });


        }

        TextView textPlataforma = findViewById(R.id.text_plataforma);
        //List<Platform> listPlataformas = result.getPlatforms();


        gameDetailsViewModel.getPlatforms().observe(this, new Observer<List<es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform>>() {
            @Override
            public void onChanged(List<es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform> platforms) {
                String platform="";
                if(platforms!=null) {
                    for(Platform p: platforms){
                        if(p.getIDGame().intValue() == result.getId().intValue()){
                            platform += p.getName() + "   ";
                        }
                    }
                }
                textPlataforma.setText(platform) ;
            }
        });



        TextView textPuntuacion = findViewById(R.id.text_puntuacion);
        if(result.getRating()!=null) textPuntuacion.setText("Puntuación " + result.getRating().toString() + "/5");





        bAddCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAdd[0] = !clickAdd[0];
                if (addGame[0]){
                    //gameDetailsViewModel.insertCompleted(usersCompleted);
                    bAddCompleted.setTextColor(Color.parseColor("#8B1B1B"));
                    addGame[0] = false;
                }
                else{
                   // gameDetailsViewModel.deleteCompleted(idu, idg);
                    bAddCompleted.setTextColor(Color.parseColor("#353535"));
                    addGame[0] = true;
                }
            }
        });


        bAddWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAdd[1] = !clickAdd[1];
                if (addGame[1]){
                    //gameDetailsViewModel.insertWishlist(usersWishlist);
                    bAddWishlist.setTextColor(Color.parseColor("#8B1B1B"));
                    addGame[1] = false;
                }
                else{
                    //gameDetailsViewModel.deleteWishlist(idu, idg);
                    bAddWishlist.setTextColor(Color.parseColor("#353535"));
                    addGame[1] = true;
                }
            }
        });

        bAddPlaying.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAdd[2] = !clickAdd[2];
                if (addGame[2]){
                    //gameDetailsViewModel.insertPlaying(usersPlaying);
                    bAddPlaying.setTextColor(Color.parseColor("#8B1B1B"));
                    addGame[2] = false;
                }
                else{
                    //gameDetailsViewModel.deletePlaying(idu, idg);
                    bAddPlaying.setTextColor(Color.parseColor("#353535"));
                    addGame[2] = true;
                }
            }
        });

        final ImageButton bShare = findViewById(R.id.shareButton);

        bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "¡Mira este juego!\n"+result.getName()+"\n"+result.getBackgroundImage());
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });

    }
    @Override
    protected void onDestroy() {
        float numStars;
        super.onDestroy();
        final RatingBar ratingBar = findViewById(R.id.ratingBar);
        numStars = ratingBar.getRating();
        SharedPreferences sp = getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        UsersRating ur = new UsersRating(sp.getLong("UserID", -1), ((Result) getIntent().getSerializableExtra("game")).getId() , numStars);
        gameDetailsViewModel.insertOrUpdateRating(ur);

        if(clickAdd[0]){
            if(!addGame[0]){
                gameDetailsViewModel.insertCompleted(usersCompleted);
            }
            else {
                gameDetailsViewModel.deleteCompleted(idu, idg);
            }
        }

        if(clickAdd[1]){
            if(!addGame[1]){
                gameDetailsViewModel.insertWishlist(usersWishlist);
            }
            else {
                gameDetailsViewModel.deleteWishlist(idu, idg);
            }
        }

        if(clickAdd[2]){
            if(!addGame[2]){
                gameDetailsViewModel.insertPlaying(usersPlaying);
            }
            else {
                gameDetailsViewModel.deletePlaying(idu, idg);
            }
        }
    }

    @Override
    public void onListInteraction(String url) {
        Uri webpage = Uri.parse(url);
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(webIntent);
    }
}