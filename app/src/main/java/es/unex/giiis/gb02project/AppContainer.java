package es.unex.giiis.gb02project;

import android.content.Context;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;
import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.network.ConsultaNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.network.ResultNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.roomdb.AppDatabase;
import es.unex.giiis.gb02project.ui.addgame.AddGameViewModelFactory;
import es.unex.giiis.gb02project.ui.configuracion.ConfiguracionFragment;
import es.unex.giiis.gb02project.ui.configuracion.ConfiguracionViewModelFactory;
import es.unex.giiis.gb02project.ui.editAccount.EditAccountViewModelFactory;
import es.unex.giiis.gb02project.ui.explora.ExploraViewModelFactory;
import es.unex.giiis.gb02project.ui.gamedetails.GameDetailsViewModelFactory;
import es.unex.giiis.gb02project.ui.login.LoginViewModel;
import es.unex.giiis.gb02project.ui.login.LoginViewModelFactory;
import es.unex.giiis.gb02project.ui.perfil.PerfilViewModelFactory;
import es.unex.giiis.gb02project.ui.search.SearchViewModelFactory;
import es.unex.giiis.gb02project.ui.signin.SigninViewModelFactory;

public class AppContainer {

    private AppDatabase database;
    private ResultNetworkDataSource resultNetworkDataSource;
    private ConsultaNetworkDataSource consultaNetworkDataSource;
    public ExploraRepository exploraRepository;
    public PerfilRepository perfilRepository;
    public ExploraViewModelFactory exploraFactory;
    public PerfilViewModelFactory perfilFactory;
    public ConfiguracionViewModelFactory configuracionViewModelFactory;
    public GameDetailsViewModelFactory gameDetailsViewModelFactory;
    public EditAccountViewModelFactory editAccountViewModelFactory;
    public LoginViewModelFactory loginViewModelFactory;
    public SigninViewModelFactory signinViewModelFactory;
    public AddGameViewModelFactory addGameViewModelFactory;
    public SearchViewModelFactory searchViewModelFactory;

    public AppContainer(Context context){
        database = AppDatabase.getInstance(context);
        resultNetworkDataSource = ResultNetworkDataSource.getInstance();
        consultaNetworkDataSource = ConsultaNetworkDataSource.getInstance();
        exploraRepository = ExploraRepository.getInstance(database.getImagesDao(), database.getPopularDao(), database.getNewDao(), database.getPlatformDao(), database.getPlatformCategoryDao(), database.getGenreDao(), resultNetworkDataSource, consultaNetworkDataSource);
        perfilRepository = PerfilRepository.getInstance(database.getUsersCompletedDao(), database.getUsersWishlistDao(), database.getUsersPlayingDao(), database.getCustomGamesDao(), database.getUsersRatingDao(), database.getUserDao(), resultNetworkDataSource);
        exploraFactory = new ExploraViewModelFactory(exploraRepository);
        perfilFactory = new PerfilViewModelFactory(perfilRepository);
        gameDetailsViewModelFactory = new GameDetailsViewModelFactory(perfilRepository);
        editAccountViewModelFactory = new EditAccountViewModelFactory(perfilRepository);
        loginViewModelFactory = new LoginViewModelFactory(perfilRepository);
        signinViewModelFactory = new SigninViewModelFactory(perfilRepository);
        addGameViewModelFactory = new AddGameViewModelFactory(perfilRepository);
        searchViewModelFactory = new SearchViewModelFactory(exploraRepository);
        configuracionViewModelFactory = new ConfiguracionViewModelFactory(perfilRepository);
    }
}
