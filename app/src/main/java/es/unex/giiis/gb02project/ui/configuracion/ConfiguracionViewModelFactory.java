package es.unex.giiis.gb02project.ui.configuracion;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.ui.editAccount.EditAccountViewModel;


public class ConfiguracionViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PerfilRepository mRepository;

    public ConfiguracionViewModelFactory(PerfilRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ConfiguracionViewModel(mRepository);
    }
}