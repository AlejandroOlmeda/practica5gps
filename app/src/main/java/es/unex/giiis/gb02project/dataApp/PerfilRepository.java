package es.unex.giiis.gb02project.dataApp;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.network.ResultNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.roomdb.CustomGamesDao;
import es.unex.giiis.gb02project.dataApp.roomdb.ImagesDao;
import es.unex.giiis.gb02project.dataApp.roomdb.NewDao;
import es.unex.giiis.gb02project.dataApp.roomdb.PopularDao;
import es.unex.giiis.gb02project.dataApp.roomdb.UserDao;
import es.unex.giiis.gb02project.dataApp.roomdb.UsersCompletedDao;
import es.unex.giiis.gb02project.dataApp.roomdb.UsersPlayingDao;
import es.unex.giiis.gb02project.dataApp.roomdb.UsersRatingDao;
import es.unex.giiis.gb02project.dataApp.roomdb.UsersWishlistDao;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersRating;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;


public class PerfilRepository {
    private static final String LOG_TAG = PerfilRepository.class.getSimpleName();

    // For Singleton instantiation
    private static PerfilRepository sInstance;
    private final MutableLiveData<Long> userFilterLiveData = new MutableLiveData<>();
/*
    private final MutableLiveData<List<UsersCompleted>> mUserCompleted = new MutableLiveData<>();
    private final MutableLiveData<List<UsersWishlist>> mUserWishlis = new MutableLiveData<>();
    private final MutableLiveData<List<UsersPlaying>> mUserPlaying = new MutableLiveData<>();

 */

    private final UsersCompletedDao mUsersCompletedDao;
    private final UsersWishlistDao mUsersWishlistDao;
    private final UsersPlayingDao mUsersPlayingDao;
    private final UsersRatingDao mUsersRatingDao;
    private final CustomGamesDao mUsersCustomsDao;
    private final ResultNetworkDataSource mResultNetworkDataSource;
    private final UserDao mUserDao;

    //private final ResultNetworkDataSource mResultNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    //private final MutableLiveData<String> userFilterLiveData = new MutableLiveData<>(); //ATENCION - mutableLiveData porque se le puede poner el valor que yo quiera
    //private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private long lastUpdateTimeMillis;
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 20000;

    private PerfilRepository(UsersCompletedDao usersCompletedDao, UsersWishlistDao usersWishlistDao, UsersPlayingDao usersPlayingDao, CustomGamesDao usersCustomsDao, UsersRatingDao usersRatingDao, UserDao userDao, ResultNetworkDataSource resultNetworkDataSource) {
        mUsersCompletedDao = usersCompletedDao;
        mUsersWishlistDao = usersWishlistDao;
        mUsersPlayingDao = usersPlayingDao;
        mUsersCustomsDao = usersCustomsDao;
        mUsersRatingDao = usersRatingDao;
        mUserDao = userDao;
        mResultNetworkDataSource = resultNetworkDataSource;
    }

    public synchronized static PerfilRepository getInstance(UsersCompletedDao usersCompletedDao, UsersWishlistDao usersWishlistDao, UsersPlayingDao usersPlayingDao, CustomGamesDao usersCustomsDao, UsersRatingDao usersRatingDao, UserDao userDao, ResultNetworkDataSource resultNetworkDataSource) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new PerfilRepository(usersCompletedDao, usersWishlistDao, usersPlayingDao, usersCustomsDao, usersRatingDao, userDao, resultNetworkDataSource);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setUser(final long user) {
        userFilterLiveData.setValue(user);
        //getCurrentUsersCompleted();
    }

    /**
     * Database related operations
     **/

    public void insertCompleted(UsersCompleted usersCompleted) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersCompletedDao.insert(usersCompleted);
            }
        });
    }

    public void deleteCompleted(long idUser, Integer idGame) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersCompletedDao.deleteGameFromCompleted(idUser, idGame);
            }
        });
    }


    public void insertWishlist(UsersWishlist usersWishlist) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersWishlistDao.insert(usersWishlist);
            }
        });
    }

    public void deleteWishlist(long idUser, Integer idGame) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersWishlistDao.deleteGameFromWishlist(idUser, idGame);
            }
        });
    }


    public void insertPlaying(UsersPlaying usersPlaying) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersPlayingDao.insert(usersPlaying);
            }
        });
    }

    public void deletePlaying(long idUser, Integer idGame) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersPlayingDao.deleteGameFromPlaying(idUser, idGame);
            }
        });
    }

    public LiveData<UsersRating> getRatingGameUser(long iduser, long idgame) {
        return mUsersRatingDao.getRowByIdsLD(iduser, idgame);
    }

    public void insertOrUpdateRating(UsersRating usersRating) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                UsersRating ur = mUsersRatingDao.getRowByIds(usersRating.getIDUser(), usersRating.getIDGame());
                if (ur!= null)
                    mUsersRatingDao.update(usersRating);
                else
                    mUsersRatingDao.insert(usersRating);
            }
        });
    }


    public LiveData<List<UsersCompleted>> getCurrentUsersCompleted() {
        return Transformations.switchMap(userFilterLiveData, new Function<Long, LiveData<List<UsersCompleted>>>() {
            @Override
            public LiveData<List<UsersCompleted>> apply(Long user) {
                return mUsersCompletedDao.getLiveData(user);
            }
        });
    }

    public LiveData<List<UsersWishlist>> getCurrentUsersWishlist() {
        return Transformations.switchMap(userFilterLiveData, new Function<Long, LiveData<List<UsersWishlist>>>() {
            @Override
            public LiveData<List<UsersWishlist>> apply(Long user) {
                return mUsersWishlistDao.getLiveData(user);
            }
        });
    }

    public LiveData<List<UsersPlaying>> getCurrentUsersPlaying() {
        return Transformations.switchMap(userFilterLiveData, new Function<Long, LiveData<List<UsersPlaying>>>() {
            @Override
            public LiveData<List<UsersPlaying>> apply(Long user) {
                return mUsersPlayingDao.getLiveData(user);
            }
        });
    }

    public LiveData<List<CustomGame>> getCurrentUsersCustom() {
        return Transformations.switchMap(userFilterLiveData, new Function<Long, LiveData<List<CustomGame>>>() {
            @Override
            public LiveData<List<CustomGame>> apply(Long user) {
                return mUsersCustomsDao.getLiveData(user);
            }
        });
    }

    public LiveData<List<Platform>> getCurrentPlatforms() {
        return mResultNetworkDataSource.getCurrentPlatforms();
    }

    public LiveData<List<ImagesGame>> getCurrentImages() {
        return mResultNetworkDataSource.getCurrentImages();
    }

    public void updateUser(User user) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.update(user);
            }
        });
    }

    public User getUserByUsername(String username) {
        return mUserDao.getUserByUsername(username);
    }

    public LiveData<User> getUserByUsernameLiveData(String username) {
        return mUserDao.getUserByUsernameLiveData(username);
    }

    public void insertUser(User user) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.insert(user);
            }
        });
    }

    public void deleteUser(Long idUser) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.deleteUser(idUser);
            }
        });
    }

    public void insertCustomGame(CustomGame customGame) {
        mExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUsersCustomsDao.insert(customGame);
            }
        });
    }

}
