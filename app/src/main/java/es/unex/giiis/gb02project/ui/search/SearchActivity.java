package es.unex.giiis.gb02project.ui.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.adapters.FilterAdapter;
import es.unex.giiis.gb02project.dataApp.network.ConsultaNetworkLoaderRunnable;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.ui.perfil.PerfilViewModel;

public class SearchActivity extends AppCompatActivity implements FilterAdapter.OnListInteractionListener {

    private RecyclerView recyclerView;
    private FilterAdapter mAdapter;
    private GridLayoutManager layoutManager;

    private RecyclerView recyclerView2;
    private FilterAdapter mAdapter2;
    private GridLayoutManager layoutManager2;
    private SearchViewModel searchViewModel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        final Intent resultIntent = new Intent();


        AppContainer appContainer = ((MyApplication)getApplication()).appContainer;
        searchViewModel = new ViewModelProvider(this, appContainer.searchViewModelFactory).get(SearchViewModel.class);

        searchViewModel.checkPlatformsGenres();

        TextView searchTV = findViewById(R.id.search_editText);
        Button bFilter = findViewById(R.id.apply_filter_button);
        Button bAscendant = findViewById(R.id.order_ascendant_button);
        Button bDescendant = findViewById(R.id.order_descendant_button);
        Drawable background = bAscendant.getBackground();
        Drawable background2 = bDescendant.getBackground();

        Map<String, String> paramBusqueda = new HashMap<>();



        bAscendant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(bAscendant.getBackground()==background ) {
                    bAscendant.setBackgroundColor(Color.parseColor("#82D86A"));
                    bDescendant.setBackground(background2);
                    paramBusqueda.put("ordering","rating");

                }
                else {
                    bAscendant.setBackground(background);
                    paramBusqueda.remove("ordering");
                }


            }
        });

        bDescendant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(bDescendant.getBackground()==background2 ) {
                    bDescendant.setBackgroundColor(Color.parseColor("#82D86A"));
                    bAscendant.setBackground(background);
                    paramBusqueda.put("ordering","-rating");
                }

                else {
                    bDescendant.setBackground(background2);
                    paramBusqueda.remove("ordering");
                }

            }
        });

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Map<String, String> paramBusqueda = new HashMap<>();
                paramBusqueda.put("search", searchTV.getText().toString());
                String genresIds = "";
                ArrayList<Integer> gList = (ArrayList<Integer>) resultIntent.getSerializableExtra("genresList");
                if (gList != null){
                    for (Integer i : gList) {
                        genresIds = genresIds + i.toString() + ",";
                    }
                    paramBusqueda.put("genres", genresIds);
                }
                String platformsIds = "";
                ArrayList<Integer> pList = (ArrayList<Integer>) resultIntent.getSerializableExtra("platformsList");
                if (pList != null){
                    for (Integer i : pList) {
                        platformsIds = platformsIds + i.toString() + ",";
                    }
                    paramBusqueda.put("platforms", platformsIds);
                }

                resultIntent.putExtra("search_query", (Serializable) paramBusqueda);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });



        recyclerView = (RecyclerView) findViewById(R.id.genre_recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 3, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new FilterAdapter("genresList", new ArrayList<>(),  this, this, resultIntent);

        searchViewModel.getGenres().observe(this, new Observer<List<Genre>>() {
            @Override
            public void onChanged(List<Genre> genres) {
                mAdapter.swapGenre(genres);
            }
        });



        recyclerView.setAdapter(mAdapter);


        recyclerView2 = (RecyclerView) findViewById(R.id.plata_recyclerView);
        recyclerView2.setHasFixedSize(true);
        layoutManager2 = new GridLayoutManager(this, 3, GridLayoutManager.HORIZONTAL, false);
        recyclerView2.setLayoutManager(layoutManager2);

        mAdapter2 = new FilterAdapter("platformsList", new ArrayList<>(),  this, this, resultIntent);

        searchViewModel.getPlatformCategories().observe(this, new Observer<List<PlatformCategory>>() {
            @Override
            public void onChanged(List<PlatformCategory> platformCategories) {
                mAdapter2.swapPlatform(platformCategories);
            }
        });



        recyclerView2.setAdapter(mAdapter2);
    }

    @Override
    public void onListInteraction(String url) {
      
        Uri webpage = Uri.parse(url);
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(webIntent);
    }
}