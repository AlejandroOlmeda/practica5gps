package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;

@Dao
public interface UsersWishlistDao {

    @Query("SELECT * FROM users_wishlist")
    public List<UsersWishlist> getAll();

    @Query("SELECT IDGame FROM users_wishlist WHERE IDUser = :iduser")
    public List<Long> getGamesByUser(long iduser);

    @Query("SELECT * FROM users_wishlist WHERE IDUser = :iduser ")
    public LiveData<List<UsersWishlist>> getLiveData(long iduser);

    @Query("SELECT * FROM users_wishlist WHERE IDUser = :iduser AND IDGame = :idgame")
    public UsersWishlist getRowByIds(long iduser, long idgame);

    @Insert
    public void insert(UsersWishlist usersWishlist);

    @Query("DELETE FROM users_wishlist")
    public void deleteAll();

    @Update
    public int update(UsersWishlist usersWishlist);

    @Query("DELETE FROM users_wishlist WHERE IDUser = :iduser AND IDGame = :idgame")
    public void deleteGameFromWishlist(long iduser, long idgame);
}
