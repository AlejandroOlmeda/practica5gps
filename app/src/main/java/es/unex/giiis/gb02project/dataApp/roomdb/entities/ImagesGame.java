package es.unex.giiis.gb02project.dataApp.roomdb.entities;

import android.content.Intent;
import android.provider.ContactsContract;
import android.widget.ImageButton;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "images_games")
public class ImagesGame implements Serializable {


    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "id";
    @Ignore
    public final static String IDGAME = "idgame";
    @Ignore
    public final static String IMAGE = "image";


    @SerializedName("mID")
    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "ID")
    private Integer mID;



    @SerializedName("mIDGame")
    @ColumnInfo(name = "IDGame")
    private Integer mIDGame;

    @SerializedName("mImage")
    @ColumnInfo(name ="image")
    private String mImage = new String();

    @Ignore
     public ImagesGame(Integer mIDGame, String mImage) {
        this.mIDGame = mIDGame;
        this.mImage = mImage;
    }

    public ImagesGame(Integer mID, Integer mIDGame, String mImage) {
        this.mID = mID;
        this.mIDGame = mIDGame;
        this.mImage = mImage;
    }

    // Create a new PopularGame from data packaged in an Inten
    @Ignore
    public ImagesGame(Intent intent) {
        mID = intent.getIntExtra(ImagesGame.ID,0);
        mIDGame = intent.getIntExtra(ImagesGame.IDGAME,0);
        mImage = intent.getStringExtra(ImagesGame.IMAGE);
    }

    /////////////////////////////////////////
    public void setID(Integer mID) {
        this.mID = mID;
    }

    public void setImage(String mImage) {
        this.mImage = mImage;
    }
    ///////////////////////////////////////////////

    public Integer getID() {
        return mID;
    }

    public String getImage() {
        return mImage;
    }
    //////////////////////////////////////////

    public Integer getIDGame() {
        return mIDGame;
    }

    public void setIDGame(Integer mIDGame) {
        this.mIDGame = mIDGame;
    }

    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, Integer mID,Integer mIDGame, String mImage) {

        intent.putExtra(ImagesGame.ID, mID);
        intent.putExtra(ImagesGame.IDGAME, mIDGame);
        intent.putExtra(ImagesGame.IMAGE, mImage);

    }
}
