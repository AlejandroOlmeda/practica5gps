
package es.unex.giiis.gb02project.dataApp.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filters implements Serializable {

    @SerializedName("years")
    @Expose
    private List<Year> years = null;

    public List<Year> getYears() {
        return years;
    }

    public void setYears(List<Year> years) {
        this.years = years;
    }

}
