package es.unex.giiis.gb02project.dataApp.roomdb;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;

@Dao
public interface UsersPlayingDao {

    @Query("SELECT * FROM users_playing")
    public List<UsersPlaying> getAll();

    @Query("SELECT IDGame FROM users_playing WHERE IDUser = :iduser")
    public List<Long> getGamesByUser(long iduser);

    @Query("SELECT * FROM users_playing WHERE IDUser = :iduser ")
    public LiveData<List<UsersPlaying>> getLiveData(long iduser);

    @Query("SELECT * FROM users_playing WHERE IDUser = :iduser AND IDGame = :idgame")
    public UsersPlaying getRowByIds(long iduser, long idgame);

    @Insert
    public void insert(UsersPlaying usersPlaying);

    @Query("DELETE FROM users_playing")
    public void deleteAll();

    @Update
    public int update(UsersPlaying usersPlaying);

    @Query("DELETE FROM users_playing WHERE IDUser = :iduser AND IDGame = :idgame")
    public void deleteGameFromPlaying(long iduser, long idgame);
}

