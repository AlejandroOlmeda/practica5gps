package es.unex.giiis.gb02project.dataApp.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

//import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.model.ShortScreenshot;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.ui.gamedetails.GameDetailsActivity;

public class ResultNetworkDataSource{
    private static final String LOG_TAG = ResultNetworkDataSource.class.getSimpleName();
    private static ResultNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<PopularGames[]> mDownloadedPopularGames;
    private final MutableLiveData<NewGames[]> mDownloadedNewGames;
    private final MutableLiveData<List<ImagesGame>> mDowloadedImages;
    private final MutableLiveData<List<Platform>> mDownloadedPlatforms;
    private final MutableLiveData<List<Result>> mDownloadedSearchGames;
    //private MutableLiveData<Result[]> mResults = new MutableLiveData<>();
    private Result[] resultsAux;


    private ResultNetworkDataSource() {
        mDownloadedPopularGames = new MutableLiveData<>();
        mDownloadedNewGames = new MutableLiveData<>();
        mDownloadedPlatforms = new MutableLiveData<>();
        mDownloadedSearchGames = new MutableLiveData<>();
        mDowloadedImages = new MutableLiveData<>();
    }

    public synchronized static ResultNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new ResultNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<PopularGames[]> getCurrentPopularGames() {
        return mDownloadedPopularGames;
    }

    public LiveData<NewGames[]> getCurrentNewGames() {
        return mDownloadedNewGames;
    }

    public LiveData<List<Platform>> getCurrentPlatforms() {
        return mDownloadedPlatforms;
    }

    public LiveData<List<Result>> getCurrentSearchGames() {
        return mDownloadedSearchGames;
    }

    public LiveData<List<ImagesGame>> getCurrentImages() {
        return mDowloadedImages;
    }

    /**
     * Gets the newest repos
     */
    public void fetchPopular(Map<String, String> parametros, List<Long> completados) {
        Log.d(LOG_TAG, "Fetch result started");
        // Get data from network and pass it to LiveData

        AppExecutors.getInstance().networkIO().execute(new ResultNetworkLoaderRunnable(parametros,completados, new OnResultLoadedListener() {
            @Override
            public void onResultLoaded(List<Result> results) {
                //mResults.postValue(results.toArray(new Result[0]));
                resultsAux = results.toArray(new Result[0]);


                //Result[] rAux = mResults.getValue();
                Result[] rAux = resultsAux;

                PopularGames[] pga = new PopularGames[200];

                List<Platform> pt = new ArrayList<>();
                List<ImagesGame> iG = new ArrayList<>();

                if(rAux!=null) {
                    List<Long> auxDesc = new ArrayList<>();
                    for (int i = 0; i < rAux.length; i++) {
                        final int auxIter = i;
                        Log.d(LOG_TAG, "Popular cargados "+i);
                        auxDesc.add(new Long(rAux[i].getId()));
                        //Esta es la segunda consulta para obtener el resto de atributos
                        if(rAux[i].getPlatforms() != null) {
                            for (int j = 0; j < rAux[i].getPlatforms().size(); j++) {
                                pt.add(new Platform(rAux[i].getId(),
                                        rAux[i].getPlatforms().get(j).getPlatform().getId(),
                                        rAux[i].getPlatforms().get(j).getPlatform().getName(),
                                        Platform.TipoPopular));
                            }
                        }
                        if(rAux[i].getShortScreenshots() != null) {
                            for (ShortScreenshot sS : rAux[i].getShortScreenshots()) {
                                iG.add(new ImagesGame(rAux[i].getId(), sS.getImage()));
                            }
                        }
                        pga[i] = new PopularGames(rAux[i].getId(), rAux[i].getName(), rAux[i].getBackgroundImage(), rAux[i].getRating(), rAux[i].getDescription(), rAux[i].getBackgroundImageAdditional());
                    }

                    mDownloadedPopularGames.setValue(pga);

                    AppExecutors.getInstance().networkIO().execute(new ResultNetworkLoaderRunnable(null, auxDesc, new OnResultLoadedListener() {
                        @Override
                        public void onResultLoaded(List<Result> results) {
                            PopularGames[] pg = new PopularGames[200];
                            int j=0;
                            for(Result r: results) {
                                Log.d(LOG_TAG, "Popular cargados ********* "+j);
                                pg[j] = new PopularGames(r.getId(), r.getName(), r.getBackgroundImage(), r.getRating(), r.getDescription(), r.getBackgroundImageAdditional());
                                j++;
                            }

                            mDownloadedPopularGames.setValue(pg);
                        }
                    }));


                    mDownloadedPlatforms.setValue(pt);
                    mDowloadedImages.setValue(iG);
                }
            }
        }));

    }

    public void fetchNew(Map<String, String> parametros, List<Long> completados) {
        Log.d(LOG_TAG, "Fetch NOVEDADES started");
        // Get data from network and pass it to LiveData

        AppExecutors.getInstance().networkIO().execute(new ResultNetworkLoaderRunnable(parametros,completados, new OnResultLoadedListener() {
            @Override
            public void onResultLoaded(List<Result> results) {
                resultsAux = results.toArray(new Result[0]);

                NewGames[] ng = new NewGames[200];

                Result[] rAux = resultsAux;
                List<Platform> pt = new ArrayList<>();

                List<ImagesGame> iG = new ArrayList<>();

                if (rAux != null) {
                    List<Long> auxDesc = new ArrayList<>();
                    for (int i = 0; i < rAux.length; i++) {
                        Log.d(LOG_TAG, "Novedad cargados " + i);
                        auxDesc.add(new Long(rAux[i].getId()));
                        ng[i] = new NewGames(rAux[i].getId(), rAux[i].getName(), rAux[i].getBackgroundImage(), rAux[i].getRating(), rAux[i].getDescription(), rAux[i].getBackgroundImageAdditional());
                        if(rAux[i].getPlatforms() != null) {
                            for (int j = 0; j < rAux[i].getPlatforms().size(); j++) {
                                pt.add(new Platform(rAux[i].getId(),
                                        rAux[i].getPlatforms().get(j).getPlatform().getId(),
                                        rAux[i].getPlatforms().get(j).getPlatform().getName(),
                                        Platform.TipoNovedad));
                            }
                        }
                        if(rAux[i].getShortScreenshots() != null) {
                            for (ShortScreenshot sS : rAux[i].getShortScreenshots()) {
                                iG.add(new ImagesGame(rAux[i].getId(), sS.getImage()));
                            }
                        }
                    }


                    mDownloadedNewGames.setValue(ng);

                    AppExecutors.getInstance().networkIO().execute(new ResultNetworkLoaderRunnable(null, auxDesc, new OnResultLoadedListener() {
                        @Override
                        public void onResultLoaded(List<Result> results) {
                            NewGames[] pga = new NewGames[200];
                            int j=0;
                            for(Result r: results) {
                                Log.d(LOG_TAG, "Popular cargados ********* "+j);
                                pga[j] = new NewGames(r.getId(), r.getName(), r.getBackgroundImage(), r.getRating(), r.getDescription(), r.getBackgroundImageAdditional());
                                j++;
                            }

                            mDownloadedNewGames.setValue(pga);
                        }
                    }));

                    mDownloadedPlatforms.setValue(pt);
                    mDowloadedImages.setValue(iG);
                }
            }
        }));

    }

    public void fetchSearchGames(Map<String, String> params) {
        AppExecutors.getInstance().networkIO().execute(new ResultNetworkLoaderRunnable(params, null, new OnResultLoadedListener() {
            @Override
            public void onResultLoaded(List<Result> results) {
                List<ImagesGame> iG = mDowloadedImages.getValue();
                List<Platform> pt = mDownloadedPlatforms.getValue();

                for (Result r: results) {
                    if(r.getShortScreenshots() != null) {
                        for (ShortScreenshot sS : r.getShortScreenshots()) {
                            iG.add(new ImagesGame(r.getId(), sS.getImage()));
                        }
                    }
                    if(r.getPlatforms() != null){
                        for (int j = 0; j < r.getPlatforms().size(); j++) {
                            pt.add(new Platform(r.getId(),
                                    r.getPlatforms().get(j).getPlatform().getId(),
                                    r.getPlatforms().get(j).getPlatform().getName(),
                                    Platform.TipoOtro));
                        }
                    }
                }

                mDownloadedSearchGames.setValue(results);
                mDowloadedImages.setValue(iG);
                mDownloadedPlatforms.setValue(pt);
            }
        }));
    }

}
