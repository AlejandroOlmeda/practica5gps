package es.unex.giiis.gb02project.ui.perfil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.ui.addgame.AddGameActivity;
import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.adapters.CustomGamesAdapter;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;
import es.unex.giiis.gb02project.ui.login.LoginActivity;
import es.unex.giiis.gb02project.adapters.MyAdapter;

public class PerfilFragment extends Fragment implements MyAdapter.OnListInteractionListener, CustomGamesAdapter.OnListInteractionListener{

    private RecyclerView recyclerViewCompletados;
    private RecyclerView recyclerViewWishlist;
    private RecyclerView recyclerViewPlaying;
    private RecyclerView recyclerViewCustom;

    private MyAdapter completedAdapter;
    private MyAdapter wishlistAdapter;
    private MyAdapter playingAdapter;
    private CustomGamesAdapter customAdapter;


    private RecyclerView.LayoutManager layoutManagerCompletados;
    private RecyclerView.LayoutManager layoutManagerWishlist;
    private RecyclerView.LayoutManager layoutManagerPlaying;
    private RecyclerView.LayoutManager layoutManagerCustom;


    private RecyclerView.LayoutManager layoutManager;

    private PerfilViewModel perfilViewModel;

    private Context mContext;

    private long logged = -1;

    private final static int USER_LOGGED = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, final Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.fragment_perfil, container, false);
        final Button bIdentify = root.findViewById(R.id.button2);
        final Button bLogout = root.findViewById(R.id.CloseSession);
        final Button bAddgame = root.findViewById(R.id.bAddgame);
        mContext = getContext();
        final PerfilFragment pf = this;
        final SharedPreferences sp = getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        logged = sp.getLong("UserID", -1);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        perfilViewModel = new ViewModelProvider(this, appContainer.perfilFactory).get(PerfilViewModel.class);


        TextView tvUsername = root.findViewById(R.id.usernameView);
        TextView textCompletados = root.findViewById(R.id.text_completados);
        TextView textCustomGames = root.findViewById(R.id.text_CustomGames);
        TextView textWishList = root.findViewById(R.id.text_wishlist);
        TextView textPlaying = root.findViewById(R.id.text_playing);


        recyclerViewCompletados = (RecyclerView) root.findViewById(R.id.repolist);
        recyclerViewCompletados.setHasFixedSize(true);
        layoutManagerCompletados = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL,false);
        recyclerViewCompletados.setLayoutManager(layoutManagerCompletados);

        recyclerViewWishlist = (RecyclerView) root.findViewById(R.id.wishlist_view);
        recyclerViewWishlist.setHasFixedSize(true);
        layoutManagerWishlist= new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL,false);
        recyclerViewWishlist.setLayoutManager(layoutManagerWishlist);

        recyclerViewPlaying = (RecyclerView) root.findViewById(R.id.playinglist);
        recyclerViewPlaying.setHasFixedSize(true);
        layoutManagerPlaying = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL,false);
        recyclerViewPlaying.setLayoutManager(layoutManagerPlaying);

        recyclerViewCustom = (RecyclerView) root.findViewById(R.id.CustomGameList);
        recyclerViewCustom.setHasFixedSize(true);
        layoutManagerCustom = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL,false);
        recyclerViewCustom.setLayoutManager(layoutManagerCustom);


        completedAdapter = new MyAdapter(new ArrayList<>(), this, getContext());
        wishlistAdapter = new MyAdapter(new ArrayList<>(), this, getContext());
        playingAdapter = new MyAdapter(new ArrayList<>(), this, getContext());
        customAdapter = new CustomGamesAdapter(new ArrayList<>(),this,getContext());

        perfilViewModel.getCompleted().observe(this.getViewLifecycleOwner(), new Observer<List<UsersCompleted>>() {
            @Override
            public void onChanged(List<UsersCompleted> usersCompleted) {

                List<Result> resultsAux = new ArrayList<>();
                for(UsersCompleted uC : usersCompleted) {
                    resultsAux.add(uC.toResult());
                }
                completedAdapter.swap(resultsAux);
                textCompletados.setText(getResources().getString(R.string.text_completados) + " (" + resultsAux.size() + ")");
            }
        });

        perfilViewModel.getWishlist().observe(this.getViewLifecycleOwner(), new Observer<List<UsersWishlist>>() {
            @Override
            public void onChanged(List<UsersWishlist> usersWishlist) {

                List<Result> resultsAux = new ArrayList<>();
                for(UsersWishlist uW : usersWishlist) {
                    resultsAux.add(uW.toResult());
                }
                wishlistAdapter.swap(resultsAux);
                textWishList.setText(getResources().getString(R.string.text_wishlist) + " (" + resultsAux.size() + ")");
            }
        });

        perfilViewModel.getPlaying().observe(this.getViewLifecycleOwner(), new Observer<List<UsersPlaying>>() {
            @Override
            public void onChanged(List<UsersPlaying> usersPlayings) {

                List<Result> resultsAux = new ArrayList<>();
                for(UsersPlaying uP : usersPlayings) {
                    resultsAux.add(uP.toResult());
                }
                playingAdapter.swap(resultsAux);
                textPlaying.setText(getResources().getString(R.string.text_playing) + " (" + resultsAux.size() + ")");
            }
        });

        perfilViewModel.getCustom().observe(this.getViewLifecycleOwner(), new Observer<List<CustomGame>>() {
            @Override
            public void onChanged(List<CustomGame> usersCustoms) {
                customAdapter.swap(usersCustoms);
                textCustomGames.setText(getResources().getString(R.string.text_CustomGames) + " (" + usersCustoms.size() + ")");
            }
        });

        recyclerViewCompletados.setAdapter(completedAdapter);
        recyclerViewWishlist.setAdapter(wishlistAdapter);
        recyclerViewPlaying.setAdapter(playingAdapter);
        recyclerViewCustom.setAdapter(customAdapter);


        perfilViewModel.setIdUser(logged);

        if(logged != -1){
            bLogout.setVisibility(View.VISIBLE);
            bIdentify.setVisibility(View.INVISIBLE);
            bAddgame.setVisibility(View.VISIBLE);

            String username = sp.getString("Username", "");


            tvUsername.setText(username);


        }else{
            bLogout.setVisibility(View.INVISIBLE);
            bIdentify.setVisibility(View.VISIBLE);
            bAddgame.setVisibility(View.INVISIBLE);

        }
        bIdentify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivityForResult(intent, USER_LOGGED);
            }
        });

        bLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Borrar las shared preferences
                sp.edit().remove("UserID").commit();
                tvUsername.setText("usuario");
                bLogout.setVisibility(View.INVISIBLE);
                bIdentify.setVisibility(View.VISIBLE);
                bAddgame.setVisibility(View.INVISIBLE);
                perfilViewModel.setIdUser(-1);
            }
        });

        bAddgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, AddGameActivity.class);
                mContext.startActivity(intent);

            }
        });

        return root;
    }

    @Override
    public void onListInteraction(String url) {
        Uri webpage = Uri.parse(url);
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(webIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //View root = inflater.inflate(R.layout.fragment_perfil, container, false);

        final Button bIdentify = getActivity().findViewById(R.id.button2);
        final Button bLogout = getActivity().findViewById(R.id.CloseSession);
        final Button bAddgame = getActivity().findViewById(R.id.bAddgame);

        final SharedPreferences sp = getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        logged = sp.getLong("UserID", -1);
        if(logged != -1) {
            bLogout.setVisibility(View.VISIBLE);
            bIdentify.setVisibility(View.INVISIBLE);
            bAddgame.setVisibility(View.VISIBLE);

            String username = sp.getString("Username", "");
            TextView tvUsername = getActivity().findViewById(R.id.usernameView);
            tvUsername.setText(username);
            perfilViewModel.setIdUser(logged);

        }else{
            bLogout.setVisibility(View.INVISIBLE);
            bIdentify.setVisibility(View.VISIBLE);

        }

    }
}