package es.unex.giiis.gb02project.dataApp.roomdb.entities;

import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import es.unex.giiis.gb02project.dataApp.model.Result;

@Entity(tableName = "popular_games")
public class PopularGames implements Serializable {


    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "id";
    @Ignore
    public final static String NAME = "name";
    @Ignore
    public final static String BACKGROUNDIMAGE = "backgroundImage";
    @Ignore
    public final static String RATING = "rating";
    @Ignore
    public final static String DESCRIPTION = "description";
    @Ignore
    public final static String BACKGROUND_IMAGE_ADDITIONAL = "background_image_additional";


    @SerializedName("mID")
    @PrimaryKey
    @ColumnInfo(name = "ID")
    private Integer mID;

    @SerializedName("mName")
    @ColumnInfo(name = "name")
    private String mName = new String();

    @SerializedName("mBackgroundImage")
    @ColumnInfo(name = "backgroundImage")
    private String mBackgroundImage = new String();

    @SerializedName("mRating")
    @ColumnInfo(name = "rating")
    private Double mRating;

    @SerializedName("mDescription")
    @ColumnInfo(name ="description")
    private String mDescription = new String();

    @SerializedName("mBackground_image_additional")
    @ColumnInfo(name ="background_image_additional")
    private String mBackground_image_additional = new String();


    public PopularGames(Integer mID, String mName, String mBackgroundImage, Double mRating, String mDescription, String mBackground_image_additional) {
        this.mID = mID;
        this.mName = mName;
        this.mBackgroundImage = mBackgroundImage;
        this.mRating = mRating;
        this.mDescription = mDescription;
        this.mBackground_image_additional = mBackground_image_additional;
    }

       // Create a new PopularGame from data packaged in an Intent
    @Ignore
    public PopularGames(Intent intent) {
        mID = intent.getIntExtra(PopularGames.ID,0);
        mName = intent.getStringExtra(PopularGames.NAME);
        mBackgroundImage = intent.getStringExtra(PopularGames.BACKGROUNDIMAGE);
        mRating = intent.getDoubleExtra(PopularGames.RATING, 0);
        mDescription = intent.getStringExtra(PopularGames.DESCRIPTION);
        mBackground_image_additional = intent.getStringExtra(PopularGames.BACKGROUND_IMAGE_ADDITIONAL);
    }

    /////////////////////////////////////////
    public void setID(Integer mID) {
        this.mID = mID;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setBackgroundImage(String mBackgroundImage) {
        this.mBackgroundImage = mBackgroundImage;
    }

    public void setRating(Double mRating) {
        this.mRating = mRating;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setBackground_image_additional(String mBackground_image_additional) {
        this.mBackground_image_additional = mBackground_image_additional;
    }
    ///////////////////////////////////////////////

    public Integer getID() {
        return mID;
    }

    public String getName() {
        return mName;
    }

    public String getBackgroundImage() {
        return mBackgroundImage;
    }

    public Double getRating() {
        return mRating;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getBackground_image_additional() {
        return mBackground_image_additional;
    }
    //////////////////////////////////////////

    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, Integer mID, String mName, String mBackgroundImage, Double mRating, String mDescription, String mBackground_image_additional) {

        intent.putExtra(PopularGames.ID, mID);
        intent.putExtra(PopularGames.NAME, mName);
        intent.putExtra(PopularGames.BACKGROUNDIMAGE, mBackgroundImage);
        intent.putExtra(PopularGames.RATING, mRating);
        intent.putExtra(PopularGames.DESCRIPTION, mDescription);
        intent.putExtra(PopularGames.BACKGROUND_IMAGE_ADDITIONAL, mBackground_image_additional);

    }

    @Ignore
    public Result toResult(){
        Result result = new Result();
        result.setName(mName);
        result.setId(mID);
        result.setBackgroundImage(mBackgroundImage);
        result.setBackgroundImageAdditional(mBackground_image_additional);
        result.setDescription(mDescription);
        result.setRating(mRating);

        return result;
    }
}
