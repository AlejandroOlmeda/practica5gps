package es.unex.giiis.gb02project.dataApp.network;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.Result;

public class ResultLoaderRunnable implements Runnable {
    private final InputStream mInFile;
    private final OnResultLoadedListener mOnResultLoadedListener;

    public ResultLoaderRunnable(InputStream inFile, OnResultLoadedListener onResultLoadedListener) {
        mInFile = inFile;
        mOnResultLoadedListener = onResultLoadedListener;
    }

    @Override
    public void run() {
        // Obtención de los datos a partir del InputStream

        // Parse json file into JsonReader
        JsonReader reader = new JsonReader(new InputStreamReader(mInFile));
        // Parse JsonReader into list of Repo using Gson
        //List<ListaJuegos> listaJuegos = Arrays.asList(new Gson().fromJson(reader, ListaJuegos[].class));
        List<Result> results = new ArrayList<>();
        Result result = new Gson().fromJson(reader, Result.class);
        results.add(result);

        // Llamada al Listener con los datos obtenidos
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                mOnResultLoadedListener.onResultLoaded(results);
            }
        });


    }
}
