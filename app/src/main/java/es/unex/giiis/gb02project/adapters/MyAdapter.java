package es.unex.giiis.gb02project.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import es.unex.giiis.gb02project.ui.gamedetails.GameDetailsActivity;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.model.*;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<Result> mDataset;
    private Context mContext;



    public interface OnListInteractionListener{
        public void onListInteraction(String url);
    }

    public OnListInteractionListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView mImageView;
        public View mView;

        public Result mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            mTextView = v.findViewById(R.id.textView);
            mImageView = v.findViewById(R.id.imageGame);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Result> myDataset, OnListInteractionListener listener, Context context){
        mDataset = myDataset;
        mListener = listener;
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.mTextView.setText(mDataset.get(position).getName());


        RequestOptions requestOptions = new RequestOptions().centerCrop();

        Glide.with(mContext)
                .load(mDataset.get(position)
                .getBackgroundImage())
                .apply(requestOptions)
                .placeholder(R.drawable.loading)
                .into(holder.mImageView);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    Intent intent = new Intent(mContext, GameDetailsActivity.class);
                    intent.putExtra("game", mDataset.get(position));
                    mContext.startActivity(intent);

                    //mListener.onListInteraction(holder.mItem.getBackgroundImage());
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(Consulta dataset){
        mDataset = dataset.getResults();
        notifyDataSetChanged();
    }

    public void swap(List<Result> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }

    public void deleteData() {
        mDataset.clear();
        notifyItemRangeRemoved(0, getItemCount());
    }

}
