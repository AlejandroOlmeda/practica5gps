package es.unex.giiis.gb02project.ui.addgame;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;

public class AddGameActivity extends AppCompatActivity {

    private AddGameViewModel addGameViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_game);

        final EditText textGameName = findViewById(R.id.text_game_name);
        final EditText textGamePlatform = findViewById(R.id.text_game_platform);
        final EditText textGameRating = findViewById(R.id.text_game_rating);
        final EditText textGameGenre = findViewById(R.id.text_game_genre);
        final Button bAdd = findViewById(R.id.bInsertCustomGame);
        final Context context = this;
        SharedPreferences sp = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        long logged = sp.getLong("UserID", -1);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        addGameViewModel = new ViewModelProvider(this, appContainer.addGameViewModelFactory).get(AddGameViewModel.class);

        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String game_name = textGameName.getText().toString();
                String game_platform = textGamePlatform.getText().toString();
                String game_rating = textGameRating.getText().toString();
                String game_genre = textGameGenre.getText().toString();
                if(game_name.isEmpty() || game_platform.isEmpty() || game_rating.isEmpty() || game_genre.isEmpty() ){
                    Toast toast = Toast.makeText(context, "Rellena todos los campos", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else{
                    addGameViewModel.insert(new CustomGame(logged, game_name, game_platform, game_rating, game_genre));

                    Toast toast = Toast.makeText(context, "Juego añadido", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    finish();


                }
            }
        });
    }
}