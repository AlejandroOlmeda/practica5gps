package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersRating;

@Dao
public interface UsersRatingDao {


    @Query("SELECT * FROM users_rating WHERE IDUser = :iduser AND IDGame = :idgame")
    public LiveData<UsersRating> getRowByIdsLD(long iduser, long idgame);

    @Query("SELECT * FROM users_rating WHERE IDUser = :iduser AND IDGame = :idgame")
    public UsersRating getRowByIds(long iduser, long idgame);

    @Insert
    public void insert(UsersRating usersRating);

    @Query("DELETE FROM users_rating")
    public void deleteAll();

    @Update
    public int update(UsersRating usersRating);

    @Query("DELETE FROM users_rating WHERE IDUser = :iduser AND IDGame = :idgame")
    public void deleteGameFromRating(long iduser, long idgame);
}
