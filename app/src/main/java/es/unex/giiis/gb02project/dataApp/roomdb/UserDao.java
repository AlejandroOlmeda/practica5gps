package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users")
    public List<User> getAll();

    @Query("SELECT * FROM users WHERE username = :username")
    public User getUserByUsername(String username);

    @Query("SELECT * FROM users WHERE username = :username")
    public LiveData<User> getUserByUsernameLiveData(String username);

    @Insert
    public long insert(User user);

    @Query("DELETE FROM users")
    public void deleteAll();

    @Query("DELETE FROM users WHERE mID = :id")
    public void deleteUser(long id);

    @Update
    public int update(User user);

}
